/*

Read GPS data from input.txt and write parsed data to output.txt

Each line of the input gps data is assumed to be in the format of:
	Fix number (int), Lat (float), Long (float), Altitude [meters] (float), 
	Quality (int), Num Satellites (int), Time since last fix [ms] (int)
Each line of the output data is written in the following format:
	latitude in meters (double), longitude in meters (double)

Input data with invalid lat or invalid lon or invalid alt or low quality will be ignored

Compile:
	gcc parser.c -o parser -lm

Execute:
	./parser

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

struct GPS {
    int no;
    double latitude;
    double longitude;
    double altitude;
    int quality;
    int satellite;
    int sinceLast;
};

// online calc: http://www.csgnetwork.com/degreelenllavcalc.html
// reference: https://knowledge.safe.com/articles/725/calculating-accurate-length-in-meters-for-lat-long.html

double calcLat2Meter(double latitude)
{
	double rlat = latitude * M_PI / 180;	//reference latitude in rad
	double mPerDegLat = 111132.92 - 559.82 * cos(2* rlat) + 1.175*cos(4*rlat);
	double mlat = mPerDegLat * latitude;
	return mlat;
}

double calcLon2Meter(double latitude, double longitude)
{
	double rlat = latitude * M_PI / 180;	//reference latitude in rad
	double mPerDegLon = 111412.84 * cos(rlat) - 93.5 * cos(3*rlat);
	double mlon = mPerDegLon * longitude;
	return mlon;
}

int main(int argc, char* argv[])
{
    char readBuf[200];
    struct GPS gps;

    FILE *pf = NULL;

    if ((pf = fopen("input.txt", "r")) == NULL){
        printf("could not open source file\n");
        return 1;
    }

    FILE *of = NULL;
    if((of = fopen("output.txt", "w")) == NULL){
    	printf("could not create output file\n");
    	return 1;
    }

    while ((fgets(readBuf, sizeof (readBuf), pf))) {
        if ((sscanf(readBuf, "%d, %lf, %lf, %lf, %d, %d, %d", 
        	&gps.no, &gps.latitude, &gps.longitude, &gps.altitude, 
        	&gps.quality, &gps.satellite, &gps.sinceLast)) == 7) 
        {
            //printf ( "%d, %lf, %lf, %lf, %d, %d, %d\n", gps.no, gps.latitude, gps.longitude, gps.altitude, 
        	//gps.quality, gps.satellite, gps.sinceLast);

        	if(gps.latitude != gps.latitude)	// lat is nan
        		continue;
        	if(gps.longitude != gps.longitude)	// lon is nan
        		continue;
        	if(gps.altitude != gps.altitude)	// alt is nan
        		continue;
        	if(gps.quality < 1)	// quality too low
        		continue;

        	double mlat = calcLat2Meter(gps.latitude);
        	double mlon = calcLon2Meter(gps.latitude, gps.longitude);
        	fprintf(of, "%lf, %lf\n", mlat, mlon);
        }
    }
    fclose(pf);
    fclose(of);
    return 0;
}