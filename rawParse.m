% Change filepath accordingly! Input file is the raw log.txt
rawFile = "~/Documents/18647/Project/gps-parser/input.txt";

% Read log into table
Log = readtable(rawFile,'ReadVariableNames',false);
Log.Properties.VariableNames = {'FixNum' 'Lat' 'Lon' 'Alt' 'Quality' ...
    'Satellites' 'TSLF'};

% add columns to store calc result
% rlat = lat in rad
% mPerDegLat = meters per degree for lat
% mPerDegLon = meters per degree for lon
% mlat = lat in meters, mlon = lon in meters
% formula ref: https://knowledge.safe.com/articles/725/calculating-accurate-length-in-meters-for-lat-long.html
% online calc (precision maybe different): http://www.csgnetwork.com/degreelenllavcalc.html
Log.rlat = Log.Lat * pi / 180;
Log.mPerDegLat = 111132.92 - 559.82 * cos(2*Log.rlat) + 1.175*cos(4*Log.rlat);
Log.mPerDegLon = 111412.84 * cos(Log.rlat) - 93.5 * cos(3*Log.rlat);
Log.mlat = Log.mPerDegLat .* Log.Lat;
Log.mlon = Log.mPerDegLon .* Log.Lon;

scatter(Log.mlon, Log.mlat);